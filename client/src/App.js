import React, { Fragment } from "react";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";

import "materialize-css/sass/materialize.scss";

import NavbarTop from "./main/layout/Navbar/NavbarTop";
import Landing from "./main/layout/Landing/Landing";
import Footer from "./main/layout/Footer/Footer";
import MyApps from "./main/layout/my-apps/MyApps";

// sistema para cargar mi cv y alojar el archivo en server
// crear pagina para redireccionar a todas mis apps - falta
// implementar parallax - 1er intento
// implementar imagenes en la parte superior de las cards - hecho
// organizar distribucion de los links de la barra de navegacion
// Mejoramiento de estilos css, adaptar con curso ya tomado - falta
// implentar sticky bar - hecho
// Animaciones de acuerdo al estado del scrollbar -
// Efecto on hover suavizado para las cards
// necesito un scrollspy !!! yaaaa
// añadir mismo efecto tinder del curso de c# y angular
// Mejorar estilos de las imágenes de las cards
// Implementar internacionalización

const history = createBrowserHistory();

const App = () => {
  return (
    <div>
      <Router history={history}>
        <Fragment>
          <NavbarTop />
          <Route exact path="/" component={Landing} />
          <Switch>
            <Route exact path="/my-apps" component={MyApps} />
          </Switch>
          <Footer />
        </Fragment>
      </Router>
    </div>
  );
};

export default App;
