import React from "react";
import styles from "./Footer.module.scss";
import { MdPhoneAndroid, MdMailOutline, MdLocationCity } from "react-icons/md";

const Footer = () => {
  return (
    <div className={styles.footerStyles}>
      <div className="page-footer grey darken-4">
        <div className="container">
          <div className="row">
            <div className="col l6 s12">
              <h4 className="orange-text">TH7</h4>
              <p className="white-text">
                <MdMailOutline />
                th7.design@gmail.com // danielrojo1927@gmail.com
              </p>
              <p className="white-text">
                <MdPhoneAndroid /> +54 9 11 333 777 22
              </p>
              <p className="white-text">
                <MdLocationCity /> Buenos Aires, Argentina
              </p>
            </div>
            <div className="col l4 offset-l2 s12">
              <h4 className="orange-text">See !</h4>
              <ul>
                <li>
                  <a className="white-text" href="#!">
                    Link 1
                  </a>
                </li>
                <li>
                  <a className="white-text" href="#!">
                    Link 2
                  </a>
                </li>
                <li>
                  <a className="white-text" href="#!">
                    Link 3
                  </a>
                </li>
                <li>
                  <a className="white-text" href="#!">
                    Link 4
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="footer-copyright">
          <div className="container">
            <p style={{ marginLeft: "1rem" }} className="orange-text">
              © 2019 TH7 Design Studio
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
