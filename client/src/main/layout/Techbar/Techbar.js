import React, { useEffect } from "react";
import M from "materialize-css";

import {
  DiReact,
  DiAngularSimple,
  DiBitbucket,
  DiBootstrap,
  DiCss3,
  DiDatabase,
  DiDocker,
  DiDotnet,
  DiGit,
  DiHtml5,
  DiJira,
  DiJsBadge,
  DiMongodb,
  DiNginx,
  DiNodejs,
  DiNpm,
  DiSass
} from "react-icons/di";

import styles from "./Techbar.module.scss";

const Techbar = () => {
  useEffect(() => {
    document.addEventListener("DOMContentLoaded", function() {
      const elems = document.querySelectorAll(".carousel");
      M.Carousel.init(elems);
    });

    return () => {
      // const elems = document.querySelectorAll(".carousel");
      // const instance = M.Carousel.getInstance(elems);
      // instance.destroy();
    };
  }, []);

  return (
    <div className={styles.techbarStyles}>
      <div className="row ">
        <div className="col m5 s12">
          <h3 className="orange-text">My Stack</h3>
        </div>
        <div className="col m7 s12">
          <section>
            <DiReact title="ReactJS" />
            <DiAngularSimple title="Angular" />
            <DiHtml5 title="HTML5" />
            <DiCss3 title="CSS3" />
            <DiSass title="SASS" />
            <DiBootstrap title="Bootstrap CSS" />
            <DiJsBadge title="Javascript" />
            <DiNodejs title="NodeJS" />
            <DiNpm title="NPM" />
            <DiBitbucket title="Bitbucket" />
            <DiGit title="Git" />
            <DiDotnet title="C#" />
            <DiJira title="Jira" />
            <DiMongodb title="MongoDB" />
            <DiNginx title="NGINX" />
            <DiDocker title="Docker" />
            <DiDatabase title="Database" />
          </section>
        </div>
      </div>
    </div>
  );
};

export default Techbar;
