import React, { Fragment, useEffect } from "react";

import CardSectionTop from "../CardSectionTop/CardSectionTop";
import Techbar from "../Techbar/Techbar";

import styles from "./Landing.module.scss";
import "./Landing.scss";

const Landing = () => {
  useEffect(() => {
    return () => {};
  }, []);
  return (
    <Fragment>
      <div className={styles.landingStyles}>
        <h1 className="landing-title">Th7</h1>
        <h3 className="landing-subtitle">Web Portfolio</h3>
        <h6>
          <small>
            <strong>by TH7 Desing Project</strong>
          </small>
        </h6>
      </div>
      <CardSectionTop />
      <Techbar />
    </Fragment>
  );
};

export default Landing;
