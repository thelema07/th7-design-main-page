import React from "react";
import styles from "./NavbarTop.module.scss";
import { Link } from "react-router-dom";

const routes = [
  { route: "/", icon: "home" },
  { route: "/my-apps", icon: "view_module" }
];

const NavbarTop = () => {
  return (
    <div className="navbar-fixed">
      <nav>
        <div className={styles.navbarTopStyles}>
          <ul className="hide-on-med-and-down">
            {routes.map((nav, index) => (
              <li>
                <Link to={nav.route}>
                  <i className="material-icons">{nav.icon}</i>
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default NavbarTop;
