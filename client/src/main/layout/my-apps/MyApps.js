import React from "react";
// import { Link } from "react-router-dom";
import styles from "./MyApps.module.scss";
import "./MyApps.scss";

const MyApps = () => {
  return (
    <div className={styles.myAppsStyles}>
      <div className="row">
        <div className="col s12">
          <section>
            <h1 className="orange-text">My Apps</h1>
          </section>
        </div>
      </div>
      <div className="row">
        <div className="col s12 orange darken-2 link-list-body">
          <article className="">
            <ul className="">
              <li className="">
                <a
                  key={"http://157.245.10.230:8888/"}
                  href={"http://157.245.10.230:8888/"}
                >
                  Chat App
                </a>
              </li>
              <li className="">Test</li>
            </ul>
          </article>
        </div>
      </div>
    </div>
  );
};

export default MyApps;
