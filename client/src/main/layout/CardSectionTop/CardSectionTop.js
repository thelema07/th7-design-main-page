import React, { useEffect } from "react";
import M from "materialize-css";
import th1 from "../../../img/th7_logo_detail_2.png";
import th2 from "../../../img/pattern_1_detail.jpg";
import th3 from "../../../img/color_bake_logo_1.jpg";

import styles from "./CardSectionTop.module.scss";
import "./CarSectionTop.scss";

const CardSectionTop = () => {
  useEffect(() => {
    const elems = document.querySelector(".parallax");
    M.Parallax.init(elems, {
      responsiveThreshold: 0
    });

    return () => {
      // instance.destroy();
    };
  }, []);

  return (
    <div className={styles.cardSectionStyles}>
      <div className="row">
        <div className="col m4 s12 card-row">
          <section className="card grey darken-4">
            <div className="card-image">
              <img src={th1} alt="jolie" />
            </div>
            <div className="card-content">
              <article>
                <i className="material-icons">code</i>
                <h4 className="orange-text">
                  Fullstack <small>Developer</small>
                </h4>
              </article>
              <p className="white-text">
                Fullstack Developer with more than 2 years of experience, with
                passion for bits and technology, always curious and looking for
                knowledge.
              </p>
            </div>
          </section>
        </div>
        <div className="col m4 s12 card-row">
          <section className="card grey darken-4">
            <div className="card-image">
              <img src={th2} alt="jolie" />
            </div>
            <div className="card-content">
              <article>
                <i className="material-icons">stars</i>
                <h4 className="orange-text">
                  Graphic <small>Designer</small>
                </h4>
              </article>
              <p className="white-text">
                During almost 11 years, I worked as a Graphic Designer, focused
                in print and textile design, someday I had to take a step ahead,
                and take my ideas to the digital world.
              </p>
            </div>
          </section>
        </div>
        <div className="col m4 s12 card-row">
          <section className="card grey darken-4">
            <div className="card-image">
              <img src={th3} alt="jolie" />
            </div>
            <div className="card-content">
              <article>
                <i className="material-icons">extension</i>
                <h4 className="orange-text">
                  Skill <small>and Knowlegde </small>
                </h4>
              </article>
              <p className="white-text">
                Improve my abilities as Designer and Programmer at the same
                time, mixing and apply my design and knowledge , is the greatest
                goal, and add value to my work.
              </p>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

export default CardSectionTop;
